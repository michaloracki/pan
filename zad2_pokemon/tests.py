import unittest
from utils import PokemonAttack,  OutputFormatter
from api.api_handler import PokemonBattleTypeApiHandler


class TestTotalEffectiveness(unittest.TestCase):
    def setUp(self) -> None:
        self.attacker_types = ['fire', 'fighting']
        self.enemy_types = ['grass', 'ice', 'rock']
        self.incorrect = ['abcd', 'xyz']

    def test_single_attacker_single_enemy_type(self) -> None:
        ret = PokemonAttack.get_total_effectiveness([self.attacker_types[0]], [self.enemy_types[0]])
        self.assertEqual(ret, '2x')

    def test_single_attacker_multiple_enemy_type(self) -> None:
        ret = PokemonAttack.get_total_effectiveness([self.attacker_types[1]], self.enemy_types[1:])
        self.assertEqual(ret, '4x')

    def test_multiple_attacker_multiple_enemy_type(self) -> None:
        ret = PokemonAttack.get_total_effectiveness(self.attacker_types, self.enemy_types)
        self.assertEqual(ret, '0x')

    def test_incorrect_types(self) -> None:
        ret = PokemonAttack.get_total_effectiveness(self.attacker_types, self.incorrect)
        self.assertEqual(ret, '0x')
        

class TestMultiplier(unittest.TestCase):
    def setUp(self) -> None:
        self.enemy_types = set(['grass', 'ice', 'rock'])
        self.double_damage_types = ['grass', 'fire', 'fighting']
        self.half_damage_types = ['psyhic', 'water', 'normal', 'ice']
    
    def test_zero_multiplier(self) -> None:
        ret = PokemonAttack.get_multiplier(self.enemy_types, self.double_damage_types[1:], self.half_damage_types[:2])
        self.assertAlmostEqual(ret, 0)

    def test_double_multiplier_zero(self) -> None:
        ret = PokemonAttack.get_multiplier(self.enemy_types, self.double_damage_types[1:], self.half_damage_types)
        self.assertAlmostEqual(ret, 0.5)

    def test_non_zero_multiplier(self) -> None:
        ret = PokemonAttack.get_multiplier(self.enemy_types, self.double_damage_types, self.half_damage_types)
        self.assertAlmostEqual(ret, 1)


class TestFormatMultiplier(unittest.TestCase):
    def setUp(self) -> None:
        self.multiplier_cases = {
            'close_zero': 0.000001,
            'more_than_two_decimal_places': 1.125,
            'close_to_half': 1.499999999,
            'one_decimal_place': 1.3,
            'more_than_zero_low_precission': 2.000000001,
            'integer': 2
        }

    def test_close_zero(self) -> None:
        ret = OutputFormatter.format_multiplier(self.multiplier_cases['close_zero'])
        self.assertEqual(ret, '0x')

    def test_more_than_two_decimal_places(self) -> None:
        ret = OutputFormatter.format_multiplier(self.multiplier_cases['more_than_two_decimal_places'])
        self.assertEqual(ret, '1.12x')

    def test_close_to_half(self) -> None:
        ret = OutputFormatter.format_multiplier(self.multiplier_cases['close_to_half'])
        self.assertEqual(ret, '1.5x')

    def test_one_decimal_place(self) -> None:
        ret = OutputFormatter.format_multiplier(self.multiplier_cases['one_decimal_place'])
        self.assertEqual(ret, '1.3x')

    def test_more_than_zero_low_precission(self) -> None:
        ret = OutputFormatter.format_multiplier(self.multiplier_cases['more_than_zero_low_precission'])
        self.assertEqual(ret, '2x')

    def test_integer(self) -> None:
        ret = OutputFormatter.format_multiplier(self.multiplier_cases['integer'])
        self.assertEqual(ret, '2x')


class TestApiConnection(unittest.TestCase):
    def setUp(self) -> None:
        self.root_url = 'https://pokeapi.co/api/v2/'
        self.api = PokemonBattleTypeApiHandler('fire', self.root_url)

    def test_status(self) -> None:
        ret_status, ret_data = self.api.get_data()
        self.assertEqual(ret_status, 200)

    def test_data(self) -> None:
        ret_status, ret_data = self.api.get_data()
        self.assertIsNotNone(ret_data.get('damage_relations'))


if __name__ == '__main__':
    unittest.main()