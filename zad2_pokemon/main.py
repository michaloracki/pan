import argparse
from utils import InputFormatter, PokemonAttack, OutputFormatter


def run():
    parser = argparse.ArgumentParser(description='Pokemon')
    parser.add_argument('filename', type=str, help='Path to the file with input data')
    args = parser.parse_args()

    file_data = InputFormatter.get_file_lines(args.filename)
    results = PokemonAttack.get_pokemon_battles_eff_score(file_data)
    OutputFormatter.print_results(results)


if __name__ == '__main__':
    run()