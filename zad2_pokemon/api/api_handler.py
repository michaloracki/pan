import requests
from abc import ABC, abstractmethod


class ApiHandler(ABC):
    def __init__(self, root_url : str, endpoint : str) -> None:
        self.root_url = root_url
        self.endpoint = endpoint

    @abstractmethod
    def get_data(self) -> tuple[int, dict]:
        pass


class PokemonBattleTypeApiHandler(ApiHandler):
    def __init__(self, pokemon_type : str, root_url : str, endpoint : str ='type') -> None:
        self.type = pokemon_type
        super().__init__(root_url, endpoint)

    def get_data(self) -> tuple[int, dict]:
        r = requests.get(f'{self.root_url}/{self.endpoint}/{self.type}')
        return (r.status_code, r.json())
