from api.api_handler import PokemonBattleTypeApiHandler
import math


class PokemonAttack:
    @staticmethod
    def get_total_effectiveness(attacker_types: list[str], enemy_types: list[str] = None, damage_relation_cache : dict = None) -> str:
        if enemy_types is None or attacker_types is None:
            return '0x'
        if damage_relation_cache is None:
            damage_relation_cache = {}
        enemy_types = set(enemy_types)
        for attack_type in attacker_types:
            attack_data = PokemonAttack.get_attack_data(attack_type, damage_relation_cache)
            if attack_data is None:
                return '0x'
            if damage_relation_cache.get(attack_type) is None:
                damage_relation_cache[attack_type] = attack_data
            double_damage_types = PokemonAttack.get_types_double_damage_to(attack_data)
            half_damage_types = PokemonAttack.get_types_half_damage_to(attack_data)
            if len(enemy_types.intersection(half_damage_types + double_damage_types)) == 0:
                return '0x'

        multiplier = PokemonAttack.get_multiplier(enemy_types, double_damage_types, half_damage_types)

        return OutputFormatter.format_multiplier(multiplier)

    @staticmethod
    def get_multiplier(enemy_types : set[str], double_damage_types : list[str], half_damage_types : list[str]) -> float:
        multiplier = 1.0
        half_multiplier = len(enemy_types.intersection(half_damage_types))
        if (double_multiplier := len(enemy_types.intersection(double_damage_types))) == len(enemy_types):
            multiplier *= 2 * double_multiplier
        elif double_multiplier == 0:
            multiplier *= 0.5 * half_multiplier
        else:
            multiplier *= 2 * double_multiplier * 0.5 * half_multiplier

        return multiplier

    @staticmethod
    def get_pokemon_battles_eff_score(file_lines : list[str]) -> list[str]:
        scores = []
        damage_relation_cache = {}
        
        for line in file_lines:
            battleground = line.split()
            try:
                idx = battleground.index('->')
            except ValueError:
                continue
            
            attack_types = battleground[:idx]
            enemy_types = battleground[idx + 1:]
            scores.append(PokemonAttack.get_total_effectiveness(attack_types, enemy_types, damage_relation_cache))

        return scores
        
    @staticmethod
    def get_attack_data(pokemon_type : str, local_stored_types : dict = None, root_url : str = 'https://pokeapi.co/api/v2/') -> dict:
        if pokemon_type in local_stored_types.keys():
            return local_stored_types.get(pokemon_type)          
        api = PokemonBattleTypeApiHandler(pokemon_type, root_url)
        try:
            _, data = api.get_data()
        except Exception as e:
            return None
        return data.get('damage_relations')

    @staticmethod
    def get_types_double_damage_to(data: dict) -> list[str]:
        types = []
        for pokemon in data.get('double_damage_to'):
            if pokemon.get('name') is not None:
                types.append(pokemon['name'])
        return types

    @staticmethod
    def get_types_half_damage_to(data: dict) -> list[str]:
        types = []
        for pokemon in data.get('half_damage_to'):
            if pokemon.get('name') is not None:
                types.append(pokemon['name'])
        return types


class OutputFormatter:
    @staticmethod
    def format_multiplier(multiplier : float) -> str:
        multiplier = round(multiplier, 2)
        if math.isclose(multiplier % 1, 0.0):
            return f'{int(multiplier)}x'

        if int((multiplier % 1) * 100) % 10 == 0:
            return f'{multiplier:.1f}x'
            
        return f'{multiplier:.2f}x'

    @staticmethod
    def save(filename : str, results : list[str]):
        with open(filename, 'w') as f:
            for result in results:
                f.write(result)
                f.write('\n')

    @staticmethod
    def print_results(results):
        if len(results) > 10:
            for result in results[:10]:
                print(result)
            print('...')
            print(results[-1])
            print('Too many results to print, saving output to results.out')
            OutputFormatter.save('output.out', results)
        else:
            for result in results[:10]:
                print(result)

class InputFormatter:
    @staticmethod
    def get_file_lines(input_file_path : str) -> list:
        lines = []
        with open(input_file_path, 'r') as f:
            lines = f.readlines()
        return lines
