# Opis pomysłu rozwiązania i napotkane problemy

* Korzystając z dwóch plików z danymi stworzyłem oddzielny plik, który zawierał tylko **Headline**, **artcileBody**, **Stance**.  
* Następnie po sprawdzeniu histogramu dla labeli, widać było znaczną przewagę pośród klasy "unrelated", więc zdecydowałem się na under-sampling.
* Użyłem przetrenowanego modelu 'bert-base-uncased' i chciałem połączyć **Headline** i **articleBody** korzystając z tokenizera, aby do klasyfikatora trafiało to w postaci:  
[CLS]<**Headline**>[SEP]<**articleBody**>[SEP][PAD][PAD][PAD] 
* Następnie po podzieleniu datasetu na treningowy i walidacyjny miałem zamiar wytrenowac model za pomocą Trainera z paczki transformers. Niestety przy różnych ustawieniach oraz przy zmianach batch_size, dostawałem błędy CUDA out of memory (testowałem u siebie lokalnie Jupyter Notebook, Google Colab oraz Gradient Notebook).


