#!/usr/bin/env python
# coding: utf-8


import pandas as pd


# # Reading data


bodies_data = pd.read_csv('bodies.csv', sep=r',', skipinitialspace=True) 
bodies_data.columns = bodies_data.columns.str.replace(' ','_')            
stance_data = pd.read_csv('stances.csv', sep=r',', skipinitialspace=True)
stance_data.columns = stance_data.columns.str.replace(' ','_')
bodies_data.head()


stance_data.head()


# # Adding article body to stance data

stance_data['articleBody'] = ""

for index in stance_data.index:
  stance_data.at[index, 'articleBody'] = bodies_data.loc[bodies_data['Body_ID'] == stance_data.at[index, 'Body_ID'], 'articleBody'].values[0]
stance_data.head()


# # Dropping unnecessary columns and reorder

stance_data.drop(['Body_ID'], axis=1, inplace=True)
columns = stance_data.columns.tolist()
columns = [columns[0]] + [columns[-1]] + [columns[1]]
stance_data = stance_data[columns]
stance_data.head()


# # Saving preprocessed file

stance_data.to_csv('train_data.csv', index=False)
stance_data.head()