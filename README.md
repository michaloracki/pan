# Zadanie 2 - Pokemon
## Instrukcja odpalenia projektu

Projekt zawiera trzy pliki wejściowe do przetestowania outputów oraz testy jednostkowe. Możliwe jest też użycie własnego pliku wejściowego.

1. Odpalanie projektu przez Docker

*  Odpalenie samego programu:  
```
docker build -t zad2:pokemon --build-arg FILENAME="main.py" --build-arg INPUT="input_battles.in" .
docker run zad2:pokemon
```

*  Odpalenie testów:
```
docker build -t zad2:pokemon-unittests --build-arg FILENAME="tests.py" .
docker run zad2:pokemon-unittests
```

2. Odpalenie lokalnie  
Wymagany Python 3.8+  
```
python -m venv venv
venv\Scripts\activate (Windows) lub source venv/bin/activate (Linux)
pip install -r requirements.txt
python main.py [-h] <input_file>
python tests.py (testy)
```

# Zadanie 3 - Fake News
## Informacje w README w folderze zad3_fake_news